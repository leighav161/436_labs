/*
 * spi.c
 *
 * * Description:
 * Example code of using the serial port on the MSP432.
 * Receives commands of either ON or OFF from the serial port to
 * turn on or off the P1.0 on board LED.
 *
 * Provided by  Scott Zuidema in lab
 *
 */

#include <uart.h>


// initializing the starting position of used buffer and read buffer
uint8_t storage_location = 0; // used in the interrupt to store new data
uint8_t read_location = 0; // used in the main application to read valid data that hasn't been read yet
int BT = 0;        // Raise flag if transmission came from bluetooth.  It sends a CR and LF at the end of the line.

// choose to send the output to either BT or CodeBlocks UART
// 1 = CB     2 = BT
int CBorBT = 1;


/*----------------------------------------------------------------
 * void writeOutput(char *string)
 *
 * Description:  This is a function similar to most serial port
 * functions like printf.  Written as a demonstration and not
 * production worthy due to limitations.
 * One limitation is poor memory management.
 * Inputs: Pointer to a string that has a string to send to the serial.
 * Outputs: Places the data on the serial output.
----------------------------------------------------------------*/
void writeOutput(char *string)
{

    int i = 0;  // Location in the char array "string" that is being written to

    if (CBorBT == 2){
    // send to bluetooth
        while(string[i] != '\0') {
            EUSCI_A2->TXBUF = string[i];
            i++;
            while(!(EUSCI_A2->IFG & BIT1));
        }
    }

    if (CBorBT == 1){
        // send to CodeBlocks UART
        while(string[i] != '\0') {
            EUSCI_A0->TXBUF = string[i];
            i++;
            while(!(EUSCI_A0->IFG & BIT1));
        }


        // send a null after every write to break out of read loop
        if(string[0] == '/0')
        {
        //send null
            EUSCI_A0->TXBUF = '\0';
            while(!(EUSCI_A0->IFG & BIT1));
        }

        while(UCA0STATW & UCBUSY);
    }







//    int tx_index = 0;
//    int length = (strlen(data)+1);
//
//    while(tx_index <= (strlen(data)+1)){
//
//        // Wait for TX buffer to be ready for new data
//
//        while(!(UCA0IFG & UCTXIFG));
//
//
//
//        // Push data to TX buffer
//
//        UCA0TXBUF = data[tx_index];
//
//
//
//        // Increment index
//
//        tx_index++;
//
//    }
//
//
//
//    // Wait until the last byte is completely sent
//
//    while(UCA0STATW & UCBUSY);
}

/*----------------------------------------------------------------
 * void readInput(char *string)
 *
 * Description:  This is a function similar to most serial port
 * functions like ReadLine.  Written as a demonstration and not
 * production worthy due to limitations.
 * One of the limitations is that it is BLOCKING which means
 * it will wait in this function until there is a \n on the
 * serial input.
 * Another limitation is poor memory management.
 * Inputs: Pointer to a string that will have information stored
 * in it.
 * Outputs: Places the serial data in the string that was passed
 * to it.  Updates the global variables of locations in the
 * INPUT_BUFFER that have been already read.
----------------------------------------------------------------*/
void readInput(char *string)
{
    int i = 0;  // Location in the char array "string" that is being written to
    char temp[BUFFER_SIZE];
    // One of the few do/while loops I've written, but need to read a character before checking to see if a \n has been read
    do
    {
        // If a new line hasn't been found yet, but we are caught up to what has been received, wait here for new data
        while(read_location == storage_location && INPUT_BUFFER[read_location] != '\n');
        temp[i] = INPUT_BUFFER[read_location];  // Manual copy of valid character into "string"
        INPUT_BUFFER[read_location] = '\0';
        i++; // Increment the location in "string" for next piece of data
        read_location++; // Increment location in INPUT_BUFFER that has been read
        if(read_location == BUFFER_SIZE)  // If the end of INPUT_BUFFER has been reached, loop back to 0
            read_location = 0;
    }
    while(temp[i-1] != '\n'); // If a \n was just read, break out of the while loop

    temp[i-1] = '\0'; // Replace the \n with a \0 to end the string when returning this function
    if (BT == 1) {
        temp[i-2] = '\0';   // just for bluetooth, the app ends statements with LF + CR instead of \n
        BT = 0;
    }
    strcpy(string, temp);
}

/*----------------------------------------------------------------
 * void EUSCIA0_IRQHandler(void)
 *
 * Description: Interrupt handler for serial communication on EUSCIA0.
 * Stores the data in the RXBUF into the INPUT_BUFFER global character
 * array for reading in the main application
 * Inputs: None (Interrupt)
 * Outputs: Data stored in the global INPUT_BUFFER. storage_location
 * in the INPUT_BUFFER updated.
----------------------------------------------------------------*/
void EUSCIA2_IRQHandler(void)   // UART bluetooth
{
    if (EUSCI_A2->IFG & BIT0)  // Interrupt on the receive line
    {
        BT = 1;
        INPUT_BUFFER[storage_location] = EUSCI_A2->RXBUF; // store the new piece of data at the present location in the buffer
        EUSCI_A2->IFG &= ~BIT0; // Clear the interrupt flag right away in case new data is ready
        storage_location++; // update to the next position in the buffer
        if(storage_location == BUFFER_SIZE) // if the end of the buffer was reached, loop back to the start
            storage_location = 0;
    }
}

void EUSCIA0_IRQHandler(void)   // UART CodeBlocks
{
    if (EUSCI_A0->IFG & BIT0)  // Interrupt on the receive line
    {
        INPUT_BUFFER[storage_location] = EUSCI_A0->RXBUF; // store the new piece of data at the present location in the buffer
        EUSCI_A0->IFG &= ~BIT0; // Clear the interrupt flag right away in case new data is ready
        storage_location++; // update to the next position in the buffer
        if(storage_location == BUFFER_SIZE) // if the end of the buffer was reached, loop back to the start
            storage_location = 0;
    }
}

void setupSerial_BT()
{

    EUSCI_A2->CTLW0 |= 1;
    EUSCI_A2->MCTLW = 0;
    EUSCI_A2->CTLW0 = 0x0081;
    EUSCI_A2->BRW = 26;
    P3->SEL0 |= 0x0c;
    P3->SEL1 &= ~0x0C;
    EUSCI_A2->CTLW0 &= ~1;

    EUSCI_A2->IFG &= ~BIT0;    // Clear interrupt
    EUSCI_A2->IE |= BIT0;      // Enable interrupt
    NVIC_EnableIRQ(EUSCIA2_IRQn);

}
void setupSerial_CB()
{
    P1->SEL0 |=  (BIT2 | BIT3); // P1.2 and P1.3 are EUSCI_A0 RX
    P1->SEL1 &= ~(BIT2 | BIT3); // and TX respectively.

    EUSCI_A0->CTLW0  = BIT0; // Disables EUSCI. Default configuration is 8N1
    EUSCI_A0->CTLW0 |= BIT7; // Connects to SMCLK BIT[7:6] = 10
    EUSCI_A0->CTLW0 |= (BIT(15)|BIT(14)|BIT(11));  //BIT15 = Parity, BIT14 = Even, BIT11 = Two Stop Bits
    EUSCI_A0->BRW = 1;  // UCBR Value from above
    EUSCI_A0->MCTLW = 0x01A1; //UCBRS (Bits 15-8) & UCBRF (Bits 7-4) & UCOS16 (Bit 0)

    EUSCI_A0->CTLW0 &= ~BIT0;  // Enable EUSCI
    EUSCI_A0->IFG &= ~BIT0;    // Clear interrupt
    EUSCI_A0->IE |= BIT0;      // Enable interrupt
    NVIC_EnableIRQ(EUSCIA0_IRQn);
}

