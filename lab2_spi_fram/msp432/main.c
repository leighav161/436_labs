#include "msp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uart.h>

#define BUFFER_SIZE 100
#define FILE_ADDRESS_LOW 1
#define FILE_ADDRESS_HIGH 2
#define FILE_LENGTH_ADDRESS_LOW 3
#define FILE_LENGTH_ADDRESS_HIGH 4
#define SIZE_PER_DIR_NUM 32
#define TITLE_START 5
#define MEMORY_USED 321
#define TOTAL_MEMORY 8192
#define NEXT_OPEN_ADD 324
#define NEXT_OPEN_ADD_HIGH 322
#define NEXT_OPEN_ADD_LOW 323


/**
 * PIN ASSIGNMENTS
 * P4.3     CS
 * P1.5     CLK
 * P1.6     MOSI
 * P1.7     MISO
 */

void writeData(uint8_t OP_CODE, uint16_t ADDR, uint8_t DATA);  //Write Data to 7 seg
uint8_t readData(uint8_t OP_CODE, uint16_t ADDR);
void setWEL(void);
void writeLineDir(char line[], int fileNum);
void writeLine(char line[]);
void readLineDir(int fileNum);
uint32_t deviceID(void);
void writeDirEntry(int addr, int length, char title[], int fileNum);
void readDirEntry(int fileNum);
void spiInit(void);
void deleteFile(int fileNum);
void readFile(int fileNum);
void readOpen(void);
void writeOpen(void);


char data[] = "\0";
char string[BUFFER_SIZE];
char title[28];
uint16_t lengthFile = 0;
int num;
int titleRecieved = 0; //Title flag
int memUsed = 0; // memory used in FRAM
int memUsedPer;
uint16_t totalDir = 0; // total files stored in directory
uint16_t nextOpenAdd = NEXT_OPEN_ADD;
char flag[15];
char output[100] = "\0";
char* temp;





void main(void)
 {
WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; // stop watchdog timer

    spiInit();
    setupSerial_CB();
    setupSerial_BT();
    __enable_irq();

    INPUT_BUFFER[0]= '\0';  // Sets the global buffer to initial condition of empty

    strcpy(flag, "\0");

    totalDir = readData(0x03, 0x00);
    //readOpen();
    writeOpen();
    memUsed = readData(0x03, MEMORY_USED);

    printf("next open: %d\n", nextOpenAdd);
    writeOutput("Program Started\n");

    int i, j;

    while(1){
        readInput(string);                  // Read the input up to \n, store in string.  This function doesn't return until \n is received
        printf("%s\n", string);
        //writeOutput("accepted");

        if(string[0] != '\0'){ // if string is not empty, check the inputed data.
            if((!(strcmp(string,"STORE"))) || (!(strcmp(flag, "STORE")))) // If command is "ON", turn on LED
            {

                if(!strcmp(string, "END"))
                {
                    writeData(0x02, FILE_LENGTH_ADDRESS_HIGH+((totalDir-1)*33), lengthFile>>8);   // write length of file to the directory location
                    writeData(0x02, FILE_LENGTH_ADDRESS_LOW+((totalDir-1)*33), lengthFile&0xFF);   // write length of file to the directory location

                    lengthFile = 0; // reset length of file variable
                    strcpy(flag, "\0"); // reset mode flag
                    titleRecieved = 0;
                    writeOutput("success\n");
                }
                else if(!(strcmp(flag, "STORE")))
                {
                    if(!titleRecieved)
                    {
                        printf("Title Send: %s\n", string);
                        writeLineDir(string, totalDir); //write Title to Directory
                        writeLine(string);
                        readLineDir(totalDir);
                        printf("%s\n", output );
                        totalDir++; //increment the counter for how many directory spots have been written to
                        writeData(0x02, 0x00, totalDir);
                        titleRecieved = 1;
                        writeData(0x02, FILE_ADDRESS_LOW+((totalDir-1)*33), nextOpenAdd&0xFF);  //write least significant 8 bits to directory
                        writeData(0x02, FILE_ADDRESS_HIGH+((totalDir-1)*33), nextOpenAdd>>8);   //write most significant 8 bits to directory
                        nextOpenAdd++;
                        lengthFile += strlen(string);
                        writeOpen();
                    }
                    else
                    {
                        lengthFile += strlen(string); // increment file length count
                        writeLine(string);  //write line to file address
                    }
                }
                else
                {
                    strcpy(flag, "STORE");
                }

            }
            else if(!(strcmp(string,"DIR")))
            {
                sprintf(output, "%d\n", totalDir);
                writeOutput(output);
                for (i = 0; i < totalDir; i++)
                {
                    output[0] = '\0';
                    for(j = 5; j < 32; j++)
                    {
                        temp = readData(0x03, (33*i)+j);
                        output[j-5] = temp;
                    }
                    sprintf(output, "%s\n", output);
                    printf("%s", output);
                    writeOutput(output);
                }
                writeOutput("\0");
            }
            else if(!(strcmp(string,"MEM")))
            {
                memUsedPer = (memUsed * 100)/TOTAL_MEMORY;
                sprintf(output, "Percentage of Memory used %d\n", memUsedPer);
                printf("%s", output);
                writeOutput(output);

            }
            else if(!(strcmp(string,"DELETE"))|| !(strcmp(flag, "DELETE"))) // If command is "OFF", turn off LED
            {
                if(!(strcmp(string, "DELETE")))
                {
                    strcpy(flag, "DELETE");
                }
                else
                {
                    num = atoi(string);
                    if(num <= totalDir)
                    {
                        deleteFile(num);
                    }
                    strcpy(flag, "\0"); // reset mode flag
                    writeOutput("Deleted\n");
                }

            }
            else if(!(strcmp(string,"READ")) || !(strcmp(flag,"READ"))) // If command is "OFF", turn off LED
            {
                if(!(strcmp(string, "READ")))
                {
                    strcpy(flag, "READ");
                }
                else
                {
                    num = atoi(string);
                    if(num <= totalDir)
                    {
                        printf("%d\n", num);
                        readFile(num);
                    }
                    strcpy(flag, "\0"); // reset mode flag
                }
                //get directory number
                    //read address stored in directory
                    //read file line function
            }
            else if(!(strcmp(string,"CLEAR"))) // If command is "OFF", turn off LED
            {
                for(i=0; i<nextOpenAdd; i++)
                {
                    writeData(0x02, i, '\0');
                }
                memUsed = 0;
                totalDir = 0;
                nextOpenAdd = NEXT_OPEN_ADD;
                writeData(0x02, 0x00, totalDir);
                writeData(0x02, MEMORY_USED, memUsed);
                writeOutput("Cleared\n");
            }

        }
    }
}

/*
 * writeDirEntry
 * INPUTS
 * addr: starting address (written to FRAM)
 * length: the number of characters in the entire file (written to FRAM)
 * title: title of the file entry (written to FRAM)
 * fileNum: number of files that have already been written (not written to FRAM)
 * OUTPUTS
 * none
 */
void writeDirEntry(int addr, int length, char title[], int fileNum){
    // write the starting address
    writeData(0x02, (fileNum * 35), addr / 1000);               // thousands
    writeData(0x02, (fileNum * 35) + 1, (addr/100) % 10);       // hundreds
    writeData(0x02, (fileNum * 35) + 2, (addr/10) % 10);        // tens
    writeData(0x02, (fileNum * 35) + 3, addr % 10);             // ones

    // write the length of the file
    writeData(0x02, (fileNum * 35) + 4, (length/100) % 10);      // hundreds
    writeData(0x02, (fileNum * 35) + 5, (length/10) % 10);       // tens
    writeData(0x02, (fileNum * 35) + 6, length % 10);            // ones

    // write title of the file
    writeLineDir(title, fileNum);
}

void readDirEntry(int fileNum){
    // read the starting address
    int startingAddr;
    startingAddr = readData(0x03,(fileNum*35)) * 1000;                         // thousands
    startingAddr = startingAddr + (readData(0x03,(fileNum*35)+1) * 100);             // hundreds
    startingAddr = startingAddr + (readData(0x03,(fileNum*35)+2) * 10);        // tens
    startingAddr = startingAddr + readData(0x03,(fileNum*35)+3);               // ones
    printf("starting address of file %d\n", startingAddr);

    // read the length of the file
    int length;
    length = readData(0x03,(fileNum*35)+4) * 100;                          // hundreds
    length = length + (readData(0x03,(fileNum*35)+5) * 10);        // tens
    length = length + readData(0x03,(fileNum*35)+6);               // ones
    printf("length of file %d\n", length);

    // read the title of the file
    readLineDir(fileNum);
}

void readFile(int fileNum)
{
    uint16_t fileAddress;
    int fileLength = 0;
    int i;
    int j = 0;
    char letter;

    fileAddress = readData(0x03, FILE_ADDRESS_HIGH+((fileNum-1)*33)) << 8;
    fileAddress |= readData(0x03, FILE_ADDRESS_LOW+((fileNum-1)*33));

    printf("fileAddresses : %d %d\n", FILE_ADDRESS_HIGH+((fileNum-1)*33), FILE_ADDRESS_LOW+((fileNum-1)*32));
    fileLength = readData(0x03, FILE_LENGTH_ADDRESS_HIGH+((fileNum-1)*33)) << 8;
    fileLength |= readData(0x03, FILE_LENGTH_ADDRESS_LOW+((fileNum-1)*33));
    printf("length : %d\n", fileLength);

    for(i=1; i<=fileLength; i++)
    {
       letter = readData(0x03, fileAddress+i);

       output[j] = letter;
       j++;
       if(letter == '\n')
       {
           printf("%s", output);
           output[j+1] = '\0';
           writeOutput(output);
           j=0;
           output[0] = '\0';
           __delay_cycles(1000);
       }
    }


}

void deleteFile(int fileNum){
    int fileLength;
    uint16_t fileAddress;
    int i, j;
    char data1;

    fileAddress = readData(0x03, FILE_ADDRESS_HIGH+((fileNum-1)*33)) << 8;
    fileAddress |= readData(0x03, FILE_ADDRESS_LOW+((fileNum-1)*33));

    fileLength = readData(0x03, FILE_LENGTH_ADDRESS_HIGH+((fileNum-1)*33)) << 8;
    fileLength |= readData(0x03, FILE_LENGTH_ADDRESS_LOW+((fileNum-1)*33));

    //write null to empty file content
    for(i=0; i<fileLength; i++)
    {
        writeData(0x02, fileAddress+i, '\0');
        memUsed--;
    }

    //Delete Directory info and shift
    for(i=fileNum; i<=(totalDir-1); i++)
    {
        for(j=0; j<=SIZE_PER_DIR_NUM; j++)
        {
            data1 = (char)readData(0x03, (i*SIZE_PER_DIR_NUM)+j);
            printf("Data: %c\n", data1);
            printf("to: %d from: %d\n", ((i-1)*SIZE_PER_DIR_NUM)+j, (i*SIZE_PER_DIR_NUM)+j);
            writeData(0x02, ((i-1)*SIZE_PER_DIR_NUM)+j, data1);
        }
    }

    for(j=0; j<SIZE_PER_DIR_NUM; j++)
    {
        writeData(0x02, j*totalDir, '/0');
        memUsed--;
    }

    totalDir--;
    writeData(0x02, 0x00, totalDir);
    writeData(0x02, MEMORY_USED, memUsed);
}

// write each line of the actual file
void writeLineDir(char line[], int fileNum){
    int size = strlen(line);
    int i;
    printf("Writing %s to %d\n", line, fileNum);
    for (i = 0; i < size; i++){
        writeData(0x02,(fileNum * 33) + i + 5,line[i]);   // Write data
        memUsed++;
    }
    for (i = size; i < 28; i++){
        if(i == size)
        {
            writeData(0x02, (fileNum*33)+i+5,'\n');
        }
        else
        {
            writeData(0x02,(fileNum * 33) + i + 5,'\0');
        }

        memUsed++;
    }

    writeData(0x02, MEMORY_USED, memUsed);
}

void writeLine(char line[]){
    int size = strlen(line);
    int i;
    for (i = 0; i < size; i++){
        writeData(0x02, nextOpenAdd,line[i]);   // Write data
        nextOpenAdd++;
        memUsed++;
    }

    writeData(0x02, nextOpenAdd, '\n');
    nextOpenAdd++;
    memUsed++;

    writeData(0x02, MEMORY_USED, memUsed);
    writeOpen();
}

void readLineDir(int fileNum){
    char letter;
    char line[28];
    int i;

    for (i = 0; i < 28; i++){
        letter = readData(0x03,(fileNum * 33) + 4 + i);   // read data
        line[i] = letter;
    }

    strcpy(output, line);
}

void writeOpen()
{
    writeData(0x02, NEXT_OPEN_ADD_LOW+((totalDir-1)*33), nextOpenAdd&0xFF);  //write least significant 8 bits to directory
    writeData(0x02, NEXT_OPEN_ADD_HIGH+((totalDir-1)*33), nextOpenAdd>>8);   //write most significant 8 bits to directory
}

void readOpen()
{
    nextOpenAdd = readData(0x03, NEXT_OPEN_ADD_HIGH) << 8;
    nextOpenAdd |= readData(0x03, NEXT_OPEN_ADD_LOW);
}


void writeData(uint8_t OP_CODE, uint16_t ADDR, uint8_t DATA){
    uint8_t ADDR_MSB = (ADDR/100) % 10;
    uint8_t ADDR_LSB = ADDR - (ADDR_MSB * 100);
    setWEL();

    P4->OUT &= ~BIT3;                   //Set CS low
    __delay_cycles(300);
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = OP_CODE;              //Send 8-bit command
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = ADDR_MSB;              //1st part of 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = ADDR_LSB;              //2nd part of 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = DATA;              //Send 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    __delay_cycles(300);
    P4->OUT |= BIT3;                    //Set CS high
}

uint8_t readData(uint8_t OP_CODE, uint16_t ADDR){
    uint8_t ADDR_MSB = (ADDR/100) % 10;
    uint8_t ADDR_LSB = ADDR - (ADDR_MSB * 100);
    P4->OUT &= ~BIT3;                   //Set CS low
    __delay_cycles(300);
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = OP_CODE;              //Send 8-bit command
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = ADDR_MSB;              //1st part of 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = ADDR_LSB;              //2nd part of 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = 0;              //Send 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    __delay_cycles(300);
    P4->OUT |= BIT3;                    //Set CS high

    return EUSCI_B0->RXBUF;
}

void setWEL(void){
    P4->OUT &= ~BIT3;                   //Set CS low
   __delay_cycles(300);
   while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
   EUSCI_B0->TXBUF = 0x06;          //Send 8-bit command
   __delay_cycles(300);
   P4->OUT |= BIT3;                    //Set CS high
}

uint32_t deviceID(void) {
    uint32_t DATA;

    P4->OUT &= ~BIT3;                   //Set CS low
    __delay_cycles(100);
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = 0b10011111;              //Send 8-bit command
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    EUSCI_B0->TXBUF = 0;              //Send 16-bit address
    while(!(EUSCI_B0->IFG & 2));        //wait till it's ready to transmit
    DATA = EUSCI_B0->RXBUF;              //Receive 8-bit data
    __delay_cycles(100);
    P4->OUT |= BIT3;                    //Set CS high

    return DATA;
}

void spiInit(void) {
    EUSCI_B0->CTLW0 = 0xEDC3;

    P1->SEL0 |=  (BIT5|BIT6|BIT7);
    P1->SEL1 &= ~(BIT5|BIT6|BIT7);


    //Configure GPIO
    P4->SEL0 &= ~BIT3;                  //Configure CS as GPIO P4.3
    P4->SEL1 &= ~BIT3;
    P4->DIR  |=  BIT3;
    P4->OUT  |=  BIT3;

    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_SWRST; // Hold EUSCI_B0 module in reset state
    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_MST|EUSCI_B_CTLW0_SYNC|EUSCI_B_CTLW0_CKPL|EUSCI_B_CTLW0_MSB;
    EUSCI_B0->CTLW0 |= EUSCI_B_CTLW0_UCSSEL_2;  // Select SMCLK as EUSCI_B0 clock
    EUSCI_B0->BRW = 0x0018; // Set BITCLK = BRCLK/ (UCBRx+1) = 3 MHz/24 = 128K
    EUSCI_B0->CTLW0 &= ~EUSCI_B_CTLW0_SWRST; // Clear SWRST to resume operation
    EUSCI_B0->CTLW0 &= ~EUSCI_B_CTLW0_CKPL;
}

