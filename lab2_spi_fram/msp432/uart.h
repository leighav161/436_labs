/*
 * spi.h
 *
 * * Description:
 * Example code of using the serial port on the MSP432.
 * Receives commands of either ON or OFF from the serial port to
 * turn on or off the P1.0 on board LED.
 *
 * Provided by  Scott Zuidema in lab
 *
 */

#ifndef UART_H_
#define UART_H_


#include "msp.h"
#include <stdio.h>

// Making a buffer of 100 characters for serial to store to incoming serial data
#define BUFFER_SIZE 100

/*-----------------------------------------------------------------
 *                          Macro Packages
 *----------------------------------------------------------------*/
#define STCSR_CLKSRC (BIT2)       // This is the CLKSOURCE bit, BIT2
#define STCSR_INT_EN (BIT1)       // This is the TICKINT bit, BIT1
#define STCSR_EN     (BIT0)       // This is the ENABLE bit, BIT0


char INPUT_BUFFER[BUFFER_SIZE];
void writeOutput(char *string);
void readInput(char *string);
void setupSerial_BT();
void setupSerial_CB();




#endif /* UART_H_ */
