/* ---------------------------------------------------------
* Authors: Kendra Francis and Leigha VanderKlok
* Date: 1-20-22
* Purpose: to interface with the MSP432 using UART.  The red
* LED on the MSP432 initially blinks at 60bpm and the user
* can increase or decrease the speed by pressing 'u' or 'd', respectively.
* The system can be reset to 60 bpm by pressing 'r'.
------------------------------------------------------------*/

#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>
#include "pc_uart.h"

#define STRING_SIZE 150
int main()
{
    HANDLE hComm;                                       // hComm stream
    FILE *fp;

    int commPort = getCommInput();                      // Ask user for comm port number
    connectToComm(commPort, &hComm);
    setSerialParams(&hComm);                            // Set the serial port parameters

    char readString[RX_BUFFER_SIZE];                    // Read Input String
    char commandStr[STRING_SIZE];
    char secondStr[STRING_SIZE];
    char fileStr[STRING_SIZE];
    char fileContent[255] = "";
    char buff[100];
    int numSend;


    while(1)
    {
        printf("Enter a command, for command list enter \"help\": ");
        scanf("%s", &commandStr);

        sprintf(commandStr, "%s\n", commandStr);

        if(strcmp(commandStr, "help") == 0)
        {
            printf("STORE\nDIR\nMEM\nDELETE\nREAD\nCLEAR\n");
        }
        else if(strcmp(commandStr, "STORE\n") == 0)
        {
            printf("Enter file name: ");
            scanf("%s", &secondStr);

            sprintf(fileStr, "./testFiles/%s.txt", secondStr);

            fp = fopen(fileStr, "r");

            writeComm(&hComm, "STORE\n");
            Sleep(1000);

            while(fgets(buff, 100, fp) != NULL)
            {
                writeComm(&hComm, buff);
                Sleep(1000);
            }
            fclose(fp);

            writeComm(&hComm, "\n");
            Sleep(1000);
            writeComm(&hComm, "END\n");
        }
        else if(strcmp(commandStr, "DIR\n") == 0)
        {
            writeComm(&hComm, commandStr);
            Sleep(1000);
            //readComm(&hComm, fileContent);
        }
        else if(strcmp(commandStr, "MEM\n") == 0)
        {
            writeComm(&hComm, commandStr);
            Sleep(1000);
            //readComm(&hComm, fileContent);
        }
        else if(strcmp(commandStr, "DELETE\n") == 0)
        {
            writeComm(&hComm, "DIR\n");

            printf("Enter file number you would like to delete: ");
            scanf("%s", &secondStr);

            int num = atoi(secondStr);
            sprintf(secondStr, "%s\n", secondStr);
            if(num <= 10 && num > 0)
            {
                writeComm(&hComm, "DELETE\n");
                Sleep(1000);
                writeComm(&hComm, secondStr);
            }
            else
            {
                printf("%s is an invalid number\n");
            }

            writeComm(&hComm, "END\n");
            Sleep(1000);
        }
        else if(strcmp(commandStr, "READ\n") == 0)
        {
            writeComm(&hComm, "DIR\n");

            printf("Enter file num: ");
            scanf("%s", &secondStr);
            sprintf(secondStr, "%s\n", secondStr);

            strcat(commandStr, secondStr);
            writeComm(&hComm, commandStr);
            Sleep(1000);
            writeComm(&hComm, "END\n");
            Sleep(1000);


         //   readComm(&hComm, fileContent);
        }
        else if(strcmp(commandStr, "CLEAR\n") == 0)
        {
            writeComm(&hComm, commandStr);
            Sleep(1000);
           // readComm(&hComm, fileContent);
        }
        else
        {
            printf("Not a valid command\n");
        }
        //printf("Read\n");
        readComm(&hComm, readString);
        numSend = atoi(readString);
        for(int i=0; i<numSend; i++)
        {
           printf("%d ", i+1);
           readComm(&hComm, readString);
        }
        //readComm(&hComm, readString);
    }

    CloseHandle(hComm);                                 // Close the Serial Port

    return 0;
}

